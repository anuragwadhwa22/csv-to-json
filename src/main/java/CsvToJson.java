import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class CsvToJson {
    public static void main(String[] args) throws Exception {

        File input = new File("Current Request - with details.csv");
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter output file path and name:");
        String outputFile = sc.next();
        File output = new File(outputFile);

        CsvSchema csvSchema = CsvSchema.builder().setUseHeader(true).build();
        CsvMapper csvMapper = new CsvMapper();

        // Read data from CSV file
        List<Object> readAll = csvMapper.readerFor(Map.class).with(csvSchema).readValues(input).readAll();

        ObjectMapper mapper = new ObjectMapper();

        // Write JSON data to output file
        mapper.writerWithDefaultPrettyPrinter().writeValue(output, readAll);

        // Print JSON file data
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(readAll));
    }
}
